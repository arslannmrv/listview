package com.example.myapp;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class listview extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
    }

    public class MainActivity extends Activity {
        final List<Kisi> kisiler=new ArrayList<Kisi>();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            kisiler.add(new Kisi("Ahmet Yılmaz", false));
            kisiler.add(new Kisi("Ayşe Küçük", true));
            kisiler.add(new Kisi("Fatma Bulgurcu", true));
            kisiler.add(new Kisi("İzzet Altınmeşe", false));
            kisiler.add(new Kisi("Melek Subaşı", true));
            kisiler.add(new Kisi("Selim Serdilli",false));
            kisiler.add(new Kisi("Halil İbrahim",false));
        }
    }



    public class Kisi {
        private String  isim;
        private boolean kadinMi;

        public Kisi(String isim, boolean kadinMi) {
            super();
            this.isim = isim;
            this.kadinMi = kadinMi;
        }

        @Override
        public String toString() {
            return isim;
        }

        public String getIsim() {
            return isim;
        }

        public void setIsim(String isim) {
            this.isim = isim;
        }

        public boolean isKadinMi() {
            return kadinMi;
        }

        public void setKadinMi(boolean kadinMi) {
            this.kadinMi = kadinMi;
        }
    }
}
