package com.example.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class intentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
    }

    public void ileri(View view) {
        Intent ıntent= new Intent(getApplicationContext(),listview.class);
        startActivity(ıntent);

    }

    public void geri(View view) {
        Intent ıntenti = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(ıntenti);
    }
}
